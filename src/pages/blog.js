import React from "react";
import { Link, graphql } from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";

const BlogPage = ({ data }) => (
  <Layout>
    <SEO title="Blog" keywords={[`gatsby`, `react`, `tutorial`, `blog`]} />
    <h1>Latest Posts</h1>
    <p>Latest posts in the blog on this site. Neat!</p>
    {data.allMarkdownRemark.edges.map(post => (
      <div key={post.node.id}>
        <h3 style={{ marginBottom: ".35em" }}>{post.node.frontmatter.title}</h3>
        <p
          style={{
            fontSize: ".825rem",
            margin: "0 auto 1em"
          }}
        >
          Posted By {post.node.frontmatter.author} on{" "}
          {post.node.frontmatter.date}
        </p>
        <Link
          style={{
            display: "inline-block",
            marginBottom: "1em"
          }}
          to={post.node.frontmatter.path}
        >
          Read more&hellip;
        </Link>
        <hr style={{ marginBottom: "2em" }} />
      </div>
    ))}
  </Layout>
);

export const pageQuery = graphql`
  query BlogIndexQuery {
    allMarkdownRemark {
      edges {
        node {
          id
          frontmatter {
            path
            title
            date
            author
          }
        }
      }
    }
  }
`;

export default BlogPage;
