import React from "react";
import { Link } from "gatsby";

import Layout from "../components/layout";
import SEO from "../components/seo";

const SecondPage = () => (
  <Layout>
    <SEO
      title="Page two"
      keywords={["gatsby", "react", "tutorial", "router"]}
    />
    <h1>Hi from the second page</h1>
    <p>Welcome to page 2</p>
    <p>
      Not as interesting as the other page, but it's a new page using the same
      template layout, so that's cool.
    </p>
    <Link to="/">Go back to the homepage</Link>
  </Layout>
);

export default SecondPage;
