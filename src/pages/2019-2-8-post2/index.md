---
path: "/post-two"
date: "2019-02-08"
title: "My Second Gatsby Post"
author: "Gary Wilpizeski"
---

# Yet Another New Blog Post

### A new post for a new blog

This is my very ~~first~~**second** blog post in Gatsby!

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ullamcorper dui sed porta tempor. Nunc scelerisque nunc in maximus tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce bibendum ultrices odio vel venenatis. In hac habitasse platea dictumst. Fusce et velit non tortor dictum tincidunt ac ac lectus. Aliquam suscipit erat nec turpis posuere, non ultricies lorem molestie.
