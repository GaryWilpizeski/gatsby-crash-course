import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";

const AboutPage = () => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `react`, `tutorial`]} />
      <div>
        <h1>About Us</h1>
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quaerat
          obcaecati pariatur sed adipisci, saepe quidem libero earum hic
          accusamus quod repellat, laboriosam, aliquam officia magni? Aut ad
          dolores nisi magni!
        </p>
      </div>
    </Layout>
  );
};

export default AboutPage;
