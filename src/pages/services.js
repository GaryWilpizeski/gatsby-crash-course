import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";

const ServicesPage = () => {
  return (
    <Layout>
      <SEO title="Home" keywords={[`gatsby`, `react`, `tutorial`]} />
      <div>
        <h1>Services</h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa
          perspiciatis nisi, repellendus enim pariatur maxime consequuntur
          aliquam temporibus, impedit et esse facere aspernatur doloribus sequi
          voluptatibus? Harum architecto totam nulla.
        </p>
        <p>
          Enim quis architecto eaque, totam dignissimos voluptates autem,
          pariatur ipsum culpa velit similique nesciunt rerum perferendis,
          provident dolorum vitae expedita aut praesentium. Vel, nostrum fuga.
          Reprehenderit itaque accusamus dicta blanditiis animi in commodi,
          facilis accusantium hic voluptatem iste ad, tenetur perspiciatis esse
          ex nobis!
        </p>
      </div>
    </Layout>
  );
};

export default ServicesPage;
